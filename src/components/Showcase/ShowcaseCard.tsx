import React from "react";

const ShowcaseCard = ({children} : {children: any}) => {
    return (
        <div className="rounded border-solid border-2 border-pink-400 w-full p-3" >
            {children}
        </div>
      );
};

export default ShowcaseCard;