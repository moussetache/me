import React from "react";
import PropTypes from "prop-types";

type props = {
  href: string,
  children: any
}

const SocialMediaButton = ({ href, children } : props) => {
  return (
        <a className="transition-opacity opacity-50 hover:opacity-100" href={href} rel="noopener noreferrer" target="_blank">
          {children}
        </a>        
  );
};

SocialMediaButton.propTypes = {
  href: PropTypes.string,
  children: PropTypes.element
};

SocialMediaButton.defaultProps = {
  href: ``,
  children: null
};

export default SocialMediaButton;
