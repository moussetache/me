import React from "react";

type props = {
    timespan: String,
    name: String,
    place: String,
    description: any
}

export default function Experience ({timespan, name, place, description} : props) {
    return (
        <li className="w-full m-auto">
            <div className="flex gap-5 text-lg ml-0 w-full m-auto relative">
                <span className="w-fit text-slate-700 dark:text-slate-300 relative">
                    {timespan}
                </span>
                <span className="inline-block relative">
                    {name}
                </span>
                <span className="inline-block relative">
                    |
                </span>
                <span className="inline-block text-slate-700 dark:text-slate-300 right-0">
                    {place}
                </span>
            </div>
            <p className="w-10/12 m-auto">
                {description}
            </p>
        </li>
    );
}