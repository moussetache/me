import React from "react";

import Logo from "@me/images/me.jpg";

import "./loading-spinner.scss";

const LoadingSpinner = () => {
    return (
        <div className="loading-spinner-container" >
            <img className="loading-spinner" src={Logo} alt="loading spinner"/> 
        </div>
      );
};

export default LoadingSpinner;