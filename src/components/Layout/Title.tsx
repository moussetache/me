import React from "react";

const Title = ({children} : {children: any}) => {
    return (
        <h2 className="mt-2 pb-5 text-xl font-bold text-left">{children}</h2>
      );
};

export default Title;