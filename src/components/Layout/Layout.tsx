import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";

import LoadingSpinner from "@components/Loading/LoadingSpinner";
import Suspense from "@components/Suspense/Suspense";

const Header = React.lazy(() => import("./Header"));

type props = {
  children: any
};

const Layout = ({ children } : props) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  return (
    <div>
      <Suspense fallback={<LoadingSpinner />}>
        <Header siteTitle={data.site.siteMetadata.title} />
      </Suspense>
      <div>
        <main className="flex flex-col items-center w-11/12 m-auto md:w-2/5 gap-5" >{children}</main>
      </div>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
