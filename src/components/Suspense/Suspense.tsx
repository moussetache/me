import React, { useEffect, useState } from "react";

type props = {
    fallback: any,
    children: any
  }

const Suspense = ({children, fallback} : props) => {
  const isSSR = typeof window === "undefined";
    const [init, setInit] = useState(false);

    useEffect(() => {
        setInit(true);
    }, []);

    if(!init) {
        return (
            <>
            {fallback}
            </>
        );
    }

    return (
        <>
        {!isSSR && (
            <React.Suspense fallback={fallback}>
                {children}
            </React.Suspense>
        )}
        </>
    );
};

export default Suspense;