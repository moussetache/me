import React from "react";

import Layout from "@components/Layout/Layout";

export default function IndexPage () {
  return (
    <Layout>
      <div className="p-3 m-auto flex flex-col items-center">

        <span className="mt-2 italic text-center">Even if I didn't have the words for it for a long time, I was always transgender. Looking back to prior decades feels like seeing all the foreshadowing in a movie for some big plot twist. In the end, you're asking yourself: how could I have missed it? </span>
        <h2 className="mt-5 text-xl font-bold text-center">Some clues that I was indeed transgender</h2>

        <ol className="mt-5 flex flex-col flex-wrap gap-3 list-decimal">
          <li>My go-to for Halloween were Disney princesses' dresses.</li>
          <li>I found sapphic relationships <em>very</em> relatable.</li>
          <li>I was envious of transgender women and wished to be one. (girl, 🤦‍♀️)</li>
          <li>My nickname was feminine and I liked it.</li>
          <li>I thought it was normal to see yourself as a stranger in the mirror.</li>
          <li>I never related to cis-het experiences.</li>
          <li>I have false memories of me wearing dresses and having long hair as a very young child.</li>
          <li>I jumped on the opportunity of growing my hair in late childhood/early adolescence (and loved it).</li>
          <li>I got scared when people started gendering me as a girl and shaved my hair.</li>
        </ol>
      </div>
    </Layout>
  );
} 
