import React from "react";

import Layout from "@components/Layout/Layout";
import SEO from "@components/Layout/Seo";

export default function NotFoundPage () {
  return (
    <Layout>
      <SEO title="404: Not found" />
      <h1>You seem lost</h1>
    </Layout>
  );
}