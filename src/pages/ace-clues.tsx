import React from "react";

import Layout from "@components/Layout/Layout";

export default function IndexPage () {
  return (
    <Layout>
      <div className="p-3 m-auto flex flex-col items-center">        
          <h2 className="mt-5 text-xl font-bold text-center">Some clues that I was indeed asexual</h2>
          <span className="mt-2 italic text-center">Non exhaustive list that I might expand over time</span>

          <ol className="mt-5 flex flex-col gap-3 list-decimal">
            <li>I never understood what was hot about anyone.</li>
            <li>I found smut in romances to be boring and awkward.</li>
            <li>I struggled to think of any celebrity I would have wanted to have sex with.</li>
            <li>I always thought sex was overrated.</li>
            <li>I fell asleep during the third <em>Fifty Shades of Grey</em> movie (that movie was so dull).</li>
            <li>I identified as cishet because I felt no attraction towards anyone and was repulsed by my own male body</li>
          </ol>
        </div>
    </Layout>
  );
} 