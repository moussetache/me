/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    './public/index.html',
    './gatsby-browser.js'
  ],
  theme: {
    extend: {},
  },
  plugins: [],
};
