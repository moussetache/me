const path = require(`path`);

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
    actions.setWebpackConfig({
      resolve: {
        alias: {
          "@components": path.resolve(__dirname, "src/components"),
          "@me": path.resolve(__dirname, "src")
        }
      }
    });
  };